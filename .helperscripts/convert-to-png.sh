fig_list="
../Tutorial/figures/title_fig
../Tutorial/phonons/2_phonopy_basics/solutions/phonopy/output/bandstructure
../Tutorial/phonons/2_phonopy_basics/solutions/phonopy/output/bandstructure_dos
../Tutorial/phonons/2_phonopy_basics/solutions/phonopy/output/thermal_properties
../Tutorial/phonons/3_convergence/solutions/8_atoms/phonopy/output/bandstructure_dos
../Tutorial/phonons/3_convergence/solutions/64_atoms/phonopy/output/bandstructure_dos
../Tutorial/phonons/4_QHA/solutions/QHA/helmholtz-volume
../Tutorial/phonons/4_QHA/solutions/QHA/volume-temperature
../Tutorial/phonons/4_QHA/solutions/QHA/thermal_expansion
../Tutorial/phonons/5_BEC/solutions/MgO_phonons/phonopy/output/bandstructure
../Tutorial/phonons/5_BEC/solutions/MgO_phonons/phonopy/output_born/bandstructure
../Tutorial/electron-phonon-coupling/6_bandgap_volume/solutions/eg_vs_tv
"
for f in $fig_list
do
  convert -density 96 $f.pdf $f.png
done
