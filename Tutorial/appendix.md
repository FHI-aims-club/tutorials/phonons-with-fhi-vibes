# Appendix: How to use V_Sim

V_Sim is a visualization software for periodic systems, which can be
used to nicely visualize phonons. In this tutorial we will only discuss
the most important settings, for more information please visit
`http://inac.cea.fr/L_Sim/V_Sim/`
Open V_Sim by typing:

```
v_sim ANIMATION_FILE
```

The `ANIMATION_FILE` is one of your `animation_*.ascii`
files. Then you should see the following two windows:

![Initial windows of V_Sim.](figures/1.jpg "fig:")     
![Initial windows of V_Sim.](figures/2.jpg "fig:")

Now you can see one unit cell, where the two Si atoms are shown as two
large green spheres. In the visualization window (first) you can rotate
the cell by using the left mouse button and zoom by using the mouse
wheel. First, you should reduce the size of the spheres for Si atoms and
show also bonds between Si atoms. 

![V_Sim visualization window (left) and settings window of the tool *set elements characteristics* (right).](figures/3.jpg "fig:")

The most important elements of the initial setting window are marked in the Figure below and are explained in the follwoing.

![V_Sim visualization window (left) and settings window of the tool *set elements characteristics* (right).](figures/4.jpg "fig:")

-  (a) Here you can change the tool. After starting V_Sim you will always
    see the tool *set element characteristics*.

- (b)  Here you can specify for which element you want to modify the
    appearance. In our case we only have Si atoms, so you do not need to
    change something here.

-  (c) Here you can change the color of the spheres.

-  (d) Here you can change the radius of the spheres.

-  (e) Click on that checkbox to activate the visualization of bonds.

-  (f) Here you can open the settings window for bonds between atoms. This
    window is shown in the next figure. You can specify the range of Si-Si distances for
    which a bond is shown (**h, i**), the color (**j**) and the
    thickness (**k**) of the bonds.

-  (g) Here you can quit the whole program.

![Settings window for bonds.](figures/5.jpg)

Next, you should not only visualize the unit cell but a larger supercell. Therefore, you first have to switch to the tool *geometry operations* (see next two figures). Then, click on the checkbox *expand nodes* (**l**) and increase the numbers of dx, dy, and dz to 1 (**m-o**).

![V_Sim visualization window (left) and settings window of the tool *geometry operations* (right).](figures/6.jpg "fig:")

![V_Sim visualization window (left) and settings window of the tool *geometry operations* (right).](figures/7.jpg "fig:")


Now, switch to the tool *phonons*. You can select a phonon mode by clicking in the respective line, i.e., you can show
phonon mode 3 by clicking at **q**. To play the animation of the
selected phonon mode click the button at **t**. If you want to see also
arrows for the atomic displacements, click on the checkbox at **p**. In
addition, you can also change the frequency (**r**) and the amplitude
(**s**) of the animation.

![V_Sim visualization window (left) and settings window of the tool *phonons* (right).](figures/8.jpg "fig:")
![V_Sim visualization window (left) and settings window of the tool *phonons* (right).](figures/9.jpg "fig:")
