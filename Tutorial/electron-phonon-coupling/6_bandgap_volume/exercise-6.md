# Exercise 6: The Role of the Lattice Expansion

*Estimated total CPU time: 15 min*

!!! warning

    In the following exercises, computational settings including the reciprocal space grid (tag ```k_grid```), the basis set, and supercell's size, have been chosen to allow for a rapid computation of the exercises in the limited time and within the CPU resources available during the tutorial session. Without loss of generality, these settings allow to demonstrate trends of the lattice dynamics of materials.
    **In the production calculation, all computational parameters should be converged.**


!!! important "In this exercise, you will:"

* Investigate how the band structure and the band gap change due to the lattice expansion.


In the previous exercises, we started from electronic structure theory
and then used it as a tool to investigate the motion of the atoms in the
harmonic approximation. Among other things, this allowed us to study the
lattice expansion as a function of temperature. Now, we go back to
electronic structure theory and investigate how the phonons affect the
electronic structure. In the first step, we will investigate how the
lattice expansion changes the electronic band gap. For this purpose, we
will now perform **electronic** band structure calculations for
geometries constructed using the lattice constants determined in the
previous exercise.

For this purpose, we have copied 'raw_volumes.dat` from the QHA
calculations, which were done in [`exercise_4`](https://fhi-aims-club.gitlab.io/tutorials/phonons-with-fhi-vibes/phonons/4_QHA/exercise-4/) to the directory
`Tutorial/electron-phonon-coupling/6_bandgap_volume/input`, where the current exercise would be performed.


Before we compute the structures of different volumes, we thin out the
volume vs. temperature data, which are stored in 10 K steps, resulting
in too many calculations for this tutorial. Use the script
`thin_out_datapoints.py` for this, which should eventually
produce a file `volume-temperature.dat` with less datapoints.

```
python thin_out_datapoints.py raw_volumes.dat
```

Next, you can use the script (`preprocess.py`) to generate the
required geometries from the lattice constants defined in
`volume-temperature.dat` in one subfolder per temperature in a
folder `working`. Please, as usual, inspect the script and
understand what it does.

```
python preprocess.py volume-temperature.dat 
```

To run all the calculations, you can use the
script `run.py`, which will run all
*FHI-aims* calculations in the working directory you created.

After all calculations are finished, collect the bandgaps with the
script `postprocess.py` which will save them to the file
`bandgaps.dat`. In this file, you will find both the the band gap
and the unit cell volume as a function of temperature:

```
# Temp [K] bandgap [eV] Volume [\AA^3]
     0.0    0.5086506600   39.88493
    50.0    0.5086138200   39.88406
   100.0    0.5084360100   39.87988
     ...
```

Use *xmgrace* to plot the band gap as a function of temperature by
calling

```
xmgrace bandgaps.dat &
```

Use *xmgrace* to plot the band gap as a function of volume by calling

```
xmgrace @-block@ bandgaps.dat @-bxy 3:2@ &
```
You can also use python script *plot_bg.py* to make the plot using python.

```
python plot_bg.py
```

![](solutions/eg_vs_tv.png)

Can you explain the observed trends? The general theory of electron-phonon interaction 
could be learned from a variety of books, e.g., Ziman's 'Electrons and Phonons'[^Ziman].

[^Ziman]: [J.M. Ziman, Electrons and Phonons: The Theory of Transport Phenomena in Solids, Published to Oxford Scholarship Online: September 2007.](https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780198507796.001.0001/acprof-9780198507796)
