import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('bandgaps.dat', comments='#')
t = data[:,0]
v = data[:,2]
eg = data[:,1]

plt.subplot(1, 2, 1)
plt.plot(t, eg)
plt.title('Band Gap vs Temperature')
plt.scatter(t, eg, color='black')
plt.xlabel('Temperature, K', fontsize=16)
plt.ylabel('Band Gap, eV', fontsize=16)

plt.subplot(1, 2, 2)
plt.plot(v, eg)
plt.title('Band Gap vs Volume')
plt.scatter(v, eg, color='black')
plt.xlabel('Volume, $\AA^{3}$', fontsize=16)
plt.ylabel('Band Gap, eV', fontsize=16)

plt.tight_layout()
plt.show()


