#!/usr/bin/env python

from ase.io import read, write
import os
import shutil
from os import listdir
from os.path import isfile, join, isdir
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt


def get_bandgap(file="aims.out", direct=False):
    """extract homo-lumo gaps (direct & indirect) from aims output file"""
    with open(file) as f:
        direct_gap = None
        for line in f:
            # run until 'ESTIMATED overall HOMO-LUMO gap:' appears
            if "ESTIMATED overall HOMO-LUMO gap:" in line:
                # get the homo-lumo
                homo_lumo = float(line.split()[4])
            # get smalles direct gap
            if "Smallest direct gap" in line:
                direct_gap = float(line.split()[5])

    if direct:
        return direct_gap or homo_lumo
    return homo_lumo

folders = sorted([f for f in listdir('./') if isdir(join('./', f))
                  and f.startswith("samples")])
print(folders)
temp = [0, 100, 200, 300, 400, 500, 600, 700, 800]
gaps = []
samples_std = []

for samples_dir in folders:
    gaps_tmp = []
    print(f'Processing directory {samples_dir}')
    os.chdir(samples_dir)
    allfiles = [f for f in listdir('./') if isfile(join('./', f)) and f.startswith("geometry.in.supercell.")]
    i = 0

    for file in allfiles:
        i += 1
        os.chdir(f'sample_{i}/aims/calculations/')
        gaps_tmp.append(get_bandgap(file="aims.out"))
        os.chdir('../../../')
    gaps.append(np.average(gaps_tmp))
    samples_std.append(np.std(gaps_tmp))
    os.chdir('../')

plt.errorbar(temp, gaps, yerr = samples_std, fmt = 'o', color = 'black',
             ecolor = 'lightblue', elinewidth = 3, capsize=3)
plt.title('Band Gap vs Temperature', fontsize=16)
plt.xlabel('Temperature, K', fontsize=16)
plt.ylabel('Band Gap, eV', fontsize=16)
plt.savefig('E_vs_T.png')
plt.show()
