"""script to run all aims calculations in the directories created from preprocess.py"""
#!/usr/bin/env python

from ase.io import read, write
import os
import shutil
from os import listdir
from os.path import isfile, join, isdir
from pathlib import Path

folders = [f for f in listdir('./') if isdir(join('./', f)) and f.startswith("samples")]
print(folders)
for samples_dir in folders:
    print(f'Processing directory {samples_dir}')
    os.chdir(samples_dir)
    allfiles = [f for f in listdir('./') if isfile(join('./', f)) and f.startswith("geometry.in.supercell.")]
    i = 0
    for file in allfiles:
        i += 1
        atoms = read(f'{file}', format='aims')
        Path(f"sample_{i}").mkdir(parents=True, exist_ok=True)
        os.chdir(f'sample_{i}')
        atoms.write('geometry.in', format='aims')
        shutil.copy('../../aims.in', './')
        os.system('vibes submit singlepoint')
        os.chdir('../')
    os.chdir('../')
    
