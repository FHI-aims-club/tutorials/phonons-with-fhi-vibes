# Electron-Phonon Coupling: Band Gap Renormalization

In the previous exercises, we have used *Phonopy* to compute the
(harmonic and quasi-harmonic) vibrational properties of silicon from the
first principles. In turn, we will now use the results achieved in
previous exercises to investigate how the vibrational effects influence
the electronic properties of silicon, and in particular, the temperature
dependence of its electronic band gap. To allow for qualitative insights
into the role of electron-phonon coupling, we will perform calculations
that explicitly account for thermodynamic changes in the lattice ([Ex. 6](/tutorials/phonons-with-fhi-vibes/electron-phonon-coupling/6_bandgap_volume/exercise-6/))
and atomic ([Ex. 7](/tutorials/phonons-with-fhi-vibes/electron-phonon-coupling/7_bandgap_temperature/exercise-7/)) degrees of freedom along the lines of
Ref. [^Skelton] and [^Ramirez], respectively. As it was
the case for the harmonic phonons, such real space approaches are not
the only option available to date to account for these effects: in
recent years, DFPT based methods have been applied successfully for
these purposes as well [^Cardona][^Giustino].

[^Skelton]: [J. M. Skelton, et al., Phys. Rev. B 89, 205203 (2014).](https://doi.org/10.1103/PhysRevB.89.205203)
[^Ramirez]: [R. Ramirez, C. Herrero, and E. Hernandez, Phys. Rev. B 73, 245202 (2006).](https://doi.org/10.1103/PhysRevB.73.245202)
[^Cardona]: [M. Cardona, Solid State Comm. 133, 3 (2005).](https://doi.org/10.1016/j.ssc.2004.10.028)
[^Giustino]: [F. Giustino, S. G. Louie, and M. L. Cohen, Phys. Rev. Lett. 105, 265501 (2010).](https://doi.org/10.1103/PhysRevLett.105.265501)
