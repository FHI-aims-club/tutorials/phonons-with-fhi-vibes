# Phonons with FHI-aims and FHI-vibes

*The tutorial is prepared by Nikita Rybin, Florian Knoop, and Christian Carbogno*

![image](./figures/title_fig.png)

This tutorial is about phonons and basics of the electron-phonon coupling. For most exercises, we will use the
[*FHI-vibes*](https://vibes-developers.gitlab.io/vibes/) -- a *Python* package for calculating, analyzing, 
and understanding the vibrational properties of solids from first principles. 

It is build on the top of the [*atomic-simulation environment* (`ASE`)
](https://wiki.fysik.dtu.dk/ase/) and utilizes
[*Phonopy* ](https://phonopy.github.io/phonopy/) functionality for the
vibrational analysis of periodic systems, and *FHI-aims* as the DFT
calculator.

You can find all data and scripts required for this tutorial here:

<https://gitlab.com/FHI-aims-club/tutorials/phonons-with-fhi-vibes>

Please first clone this repository to your local machine or compute cluster (please read [the Preparations section](preparations/) for more details).

## Outline

This tutorial consists of two parts:

1.  [Phonons: Harmonic Vibrations in Solids](phonons/theory/)

    0.  [Using *FHI-vibes* to run *FHI-aims*](phonons/0_singlepoint/exercise-0/)

    1.  [Geometry Relaxations via *FHI-vibes*](phonons/1_relax_structure/exercise-1/)

    2.  [Using *Phonopy* via *FHI-vibes* ](phonons/2_phonopy_basics/exercise-2/)

    3.  [Supercell Size Convergence](phonons/3_convergence/exercise-3/)

    4.  [Lattice Expansion: The Quasi-Harmonic Approximation](phonons/4_QHA/exercise-4/)
    
    5. [Polarization, Born Effective Charges, and Non-analytical term correction](phonons/5_BEC/exercise-5/)

2.  [Electron-Phonon Coupling: Band Gap Renormalization](electron-phonon-coupling/electron-phonon-coupling/)

    1.  [The Role of the Lattice Expansion](electron-phonon-coupling/6_bandgap_volume/exercise-6/)

    2.  [The Role of the Atomic Motion](electron-phonon-coupling/7_bandgap_temperature/exercise-7/)

In part [I](phonons/theory/), we will compute the vibrational properties of a
solid using the *harmonic approximation*. In particular, we will discuss
and investigate the convergence with respect to the supercell size used
in the calculations. Furthermore, we will learn how the harmonic
approximation can be extended in a straightforward fashion to
approximatively account for a certain degree of anharmonic
effects (quasi-harmonic approximation) and how this technique can be
used to compute the thermal lattice expansion.

In part [II](electron-phonon-coupling/electron-phonon-coupling/), we will then go back to electronic structure
theory and investigate how the fact that the nuclei are not immobile
affects the electronic band structure. Both the role of the lattice
expansion and of the atomic motion will be discussed and analyzed.

## Acknowledgments
The current version of the tutorial was designed by Nikita Rybin and Sebastian Kokott,
 based on the previous tutorials prepared by Florian Knoop,
Maja Lenz, Christian Carbogno, Martin Fuchs, Felix Hahnke, Jörg Meyer,
Karsten Rasim, Manuel Schöttler, Amrita Bhattacharya, Honghui Shang, and
Johannes Hoja. Eugen Moerman, Jakob Filser, Hagen-Henrik Kowalski, and Konstantin Lion  
extensively tested the tutorial exercises. Christian Carbogno and Volker Blum 
supervised the work.

