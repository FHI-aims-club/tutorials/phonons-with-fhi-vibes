# Exercise 0: Perform Singlepoint Calculations

!!! warning

    In the following exercises, computational settings including the reciprocal space grid (tag ```k_grid```), the basis set, and supercell's size, have been chosen to allow for a rapid computation of the exercises in the limited time and within the CPU resources available during the tutorial session. Without loss of generality, these settings allow to demonstrate trends of the lattice dynamics of materials.
    **In the production calculation, all computational parameters should be converged.**

*Estimated total CPU time: 3 min*

!!! important "In this exercise, you will learn:"

* How to set up input files of *FHI-vibes*.
* How to use the command line interface of *FHI-vibes*.
* How to perform single-point calculations with *FHI-aims* by utilizing *FHI-vibes* interface.

For this part we would use silicon (Si) structure. In the directory:

```
phonons-with-fhi-vibes/Tutorial/phonons/0_singlepoint/input
```
you will find the `geometry.in` file with a primitive unit cell of
Fm$\bar3$m-Si. Additionally you would find a file `aims.in`,
which contains the settings of the interface between
*FHI-vibes* and *FHI-aims*.

Let's inspect the input file:

```
[files]
geometry:                      geometry.in

[calculator]
name:                          aims
socketio:                      true

[calculator.parameters]
xc:                            pw-lda
compute_forces:                true

[calculator.kpoints]
density:                       3

[calculator.basissets]
Si:                            light
```

The input settings are defined by the so-called *sections*
(`[files]`, `[calculator]`)  with argument names on the left hand side, 
delimited by `:` or `=`, from the according values on
the right hand side. The right hand side can be any
JSON datatype[^1], but for us the most important are just plain strings,
numbers, and arrays.

## Section `files`

The section `files` contains a `geometry` reference to the structure we
want to investigate. In our case this is `file: geometry.in`,
which means that the input geometry is taken from `geometry.in`

## Section `calculator` and calculator options

Next sections contain all the commands you would usually write to a
*FHI-aims* `control.in` file, only that you delimit each keyword-value-pair by `:`. 
We are going to use *FHI-aims* as it is written in the
`calculator`-name line.

## Section `calculator.parameters`

These keywords correspond one-to-one to the *FHI-aims* keywords that are
written to `control.in`. Keyword-only arguments like
`vdw_correction_hirshfeld` should be given with the value true:

```
[calculator.parameters]
xc:                            pw-lda
vdw_correction_hirshfeld:      true
spin:                          collinear
use_gpu:                       true
```

## Section `calculator.kpoints`

Instead of giving a `k_grid` explicitly, *FHI-vibes* can compute
a `density` such that the density of k-points does not fall below
this value in Å$^{-3}$. This is optional, setting `k_grid` in
`calculator.parameters` is equally valid. If the `density` is
used the k-point value along each reciprocal lattice vector is computed
in proportion to the length of this lattice vector.

In order to understand how *ASE* generates `k_grid` based on `density`
variable, we could manually perform the calculations for primitive unit cell of Si.

!!! important "Manual transformation of `density` into `k_grid`"

The Si lattice vectors have the following values:

$$
a_x = [0.0, 2.74925169, 2.74925169]
$$

$$
a_y = [2.74925169, 0.0, 2.74925169]  
$$

$$
a_z = [2.74925169, 2.74925169, 0.0] 
$$

The reciprocal lattice vectors would then be:

$$
b_x = [-0.18186767, 0.18186767, 0.18186767] 
$$

$$
b_y = [0.18186767, -0.18186767, 0.18186767] 
$$

$$
b_z = [0.18186767, 0.18186767, -0.18186767] 
$$

The number of kpoints along each reciprocal lattice vector
($k_x$, $k_y$, $k_z$) is proportional to the `density` variable ($d$)
and lenght of this particular reciprocal lattice vector ($b_x$, $b_y$, $b_z$):

$$
k_x = 2 \cdot \pi \cdot |b_x| \cdot d 
$$

$$
k_y = 2 \cdot \pi \cdot |b_y| \cdot d  
$$

$$
k_z = 2 \cdot \pi \cdot |b_z| \cdot d 
$$

Note, that after the calculations using formulas above, the number of kpoints
is by default rounded to the nearest even value.

Consequently, for primitive unit cell of Si and `density` = 3 (after rounding), we 
would obtain `k_grid` = $6\times6\times6$.
 
## Section `calculator.basissets`

Here you can name the type of basis set you would like to use for your
computation. In our case, by default *FHI-vibes*, wrote `light` basis set, but for
production calculations, the basis set needs to be converged by also
trying `intermediate` and `tight`, if not `really_tight`. For the purpose of the tutorial,
we are going to utilize `light_spd` basis set. Thus, please change `light` to `light_spd`.

```
[calculator.basissets]
Si:                            light_spd

```

## Section `calculator.socketio`

Sets up socket communication via
[SocketIOCalculator](https://wiki.fysik.dtu.dk/ase/ase/calculators/socketio/socketio.html?highlight=socketio#ase.calculators.socketio.SocketIOCalculator).
This has the potential to speed up calculations since a complete restart
of *FHI-aims* after each completed SCF cycle is avoided. This feature is
optional, but recommended to use when performing calculations for
related structures, e.g., for phonon calculations as would be shown in next exercises.

The settings file, we just explored, contains all the necessary
settings to set up single-point calculations with
*FHI-vibes* using *FHI-aims* as the calculator. This file could be created
by using *FHI-vibes* CLI functionality for templates generation:

```
vibes template calculator aims
```
Results of this command were used to create `aims.in` file. 


## Run the calculation

We can run the calculations using command line interface, by typing:

```
vibes run singlepoint aims.in | tee log.aims
```

Once the calculations are done the traditional input files for
*FHI-aims* (`control.in` and `geometry.in`) as well as the
output file `aims.out` can be found in
`aims/calculations/`. Additionally, vibes produces a trajectory
file `aims/trajectory.son`, which contains all information for 
postprocessing and is particularly useful for the
adavanced tasks tackled in the next tutorials. To inspect the output
using the CLI, type:

```
vibes output trajectory aims/trajectory.son
```

which would generate a `trajectory.nc` file[^2].

## Submit calculations on a cluster
Note that it is possible to submit calculations to the queue on a
computing cluster. To submit *FHI-vibes* calcuations to your cluster
you have to set up calculations as you have done earlier on your laptop,
and submit `vibes run` command to the queue. As an example for slurm system, 
you could use:

```
#!/bin/bash -l

#SBATCH -J md|vibes
#SBATCH -o log/md.%j
#SBATCH -e log/md.%j
#SBATCH --mail-type=all
#SBATCH --mail-user=your@mail.com
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32
#SBATCH --ntasks-per-core=1
#SBATCH -t 24:0:00

vibes run singlepoint aims.in
```
Alternatively, the defaults tasks (single-point calculations, geometry optimization, 
phonons calculations, and even molecular dynamics simulations) can be submitted by specifing
slurm parameters as a section in the input file. In this case, you should add `slurm` section to the input:

```
[slurm]
name:         aims
tag:          vibes
mail_type:    none
mail_address: <userid>@rzg.mpg.de
nodes:        1
cores:        72
queue:        general
timeout:      5

```


To familiarize yourself with the  functionality of *FHI-vibes* you should visit
[*FHI-vibes* documentation
page](https://vibes-developers.gitlab.io/vibes/Tutorial/0_singlepoint/). For the time being, you could
progress with the next exercises of this tutorial.
The solutions to each exercise are stored in the folder `solutions` for each exercise.

[^1]: see e.g. <https://www.geeksforgeeks.org/json-data-types/>
[^2]: In this case trajectory.nc is generated just to show the functionality of *FHI-vibes*, we are not going to extract any tangible information from it.
