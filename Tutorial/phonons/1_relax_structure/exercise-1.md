# Exercise 1: Geometry Optimization

*Estimated total CPU time: 3 min*

!!! warning

    In the following exercises, computational settings including the reciprocal space grid (tag ```k_grid```), the basis set, and supercell's size, have been chosen to allow for a rapid computation of the exercises in the limited time and within the CPU resources available during the tutorial session. Without loss of generality, these settings allow to demonstrate trends of the lattice dynamics of materials.
    **In the production calculation, all computational parameters should be converged.**


!!! important "In this exercise, you will learn:"

* How to perform geometry relaxation in periodic systems with *FHI-aims* using *FHI-vibes*.
* How to use commands `vibes info geometry`, `vibes template option`, and `vibes info relaxation` to inspect input and output geometries and the relaxation path, generate input files, and submit calculations.


The directory:
```
phonons-with-fhi-vibes/Tutorial/phonons/1_relax_structure/input
```
contains a `geometry.in` file with the primitive unit cell of
Fm$\bar3$m-Silicon. You can get a summary of the structural information by typing:

```
vibes info geometry geometry.in
```

Carefully inspect the output. What is the space group of the system?

In order to relax the structure, we need to create input file for
*FHI-vibes*. This can be done by using *template* option shown before. 
First, we generate a template with the most general (*calculator*) information:

```
vibes template calculator aims >> relaxation.in
```

We have generated *relaxation.in*, which is input file with settings for the interface between
*FHI-vibes* and *FHI-aims*. From this point *relaxation.in* contains a set of options similar to those, 
used for singlepoint calculations. To perform geometry optimization, we should add settings related to it. 
This can be done again by using the CLI:

```
vibes template relaxation >> relaxation.in
```

Then `relaxation.in` would also contain two additional sections:

```
>>> cat relaxation.in 

... calculator sections ...

[relaxation]
driver:                        BFGS
fmax:                          0.001
unit_cell:                     True
fix_symmetry:                  False
hydrostatic_strain:            False
constant_volume:               False
scalar_pressure:               0.0
decimals:                      12
symprec:                       1e-05
workdir:                       relaxation

[relaxation.kwargs]
maxstep:                       0.2
logfile:                       relaxation.log
restart:                       bfgs.restart

```

The settings file template you just generated contains all the necessary
settings to set up and run a geometry optimization with
*FHI-vibes* using *FHI-aims* as the force/stress calculator. 

Now, we can relax the structure by running:

```
vibes run relaxation >> relaxation.out &
```

Once the relaxation is started the folder `relaxation` is generated.
This folder contains everything related to the computations. We can
verify that *FHI-aims* is indeed up and running by going to the
`relaxation/calculations` folder and by checking aims.out file (e.g. by
`tail -f aims.out`).

Once the calculations are done we can check the relaxation path
information (from the initial calculations folder):

```
vibes info relaxation relaxation/trajectory.son
```

This will give you an output as the following:

```
[trajectory]   .. set raw hash
[trajectory]   .. time elapsed: 0.000s
Relaxation info for relaxation/trajectory.son:
fmax:             1.000e+00 meV/AA
# Step |   Free energy   |   F-F(1)   | max. force |  max. stress |  Volume  |  Spacegroup  |
#      |       [eV]      |    [meV]   |  [meV/AA]  |  [meV/AA^3]  |  [AA^3]  |              |

    1    -15748.22478453    -23.700159       0.0000        50.6847     39.830   Fd-3m (227)
    2    -15748.22490123    -23.816861       0.0000         2.6193     39.739   Fd-3m (227)
    3    -15748.22489837    -23.813995       0.0000         0.0090     39.743   Fd-3m (227)
--> converged.
```

After the calculations are finished, inspect the new geometry in
`relaxation/geometry.in.next_step` with `vibes info geometry`, by typing:

```
vibes info geometry relaxation/geometry.in.next_step
```
Which parameters of the initial geometry were change? Assume, you got the initial structure from a colleague that told you they
performed a relaxation with the PBE exchange-correlation functional. Is the change in structural parameters consistent with your predictions?[^1]

After you successfully relaxed Silicon crystal you have finished this exercise, copy the relaxed
geometry to the input of Exercise 2: 

```
cp relaxation/geometry.in.next_step ../../2_phonopy_basics/input/geometry.in
```
and change to that directory (`cd ../../2_phonopy_basics/input`).


[^1]: We use LDA because it’s slightly faster, that is the only reason. In general, the dependence of any property on the underlying XC functional should be better always tested. 

