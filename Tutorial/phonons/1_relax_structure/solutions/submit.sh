#!/bin/bash -l

#SBATCH -J relaxation|vibes
#SBATCH -o log/relaxation.%j
#SBATCH -e log/relaxation.%j
#SBATCH -D ./
#SBATCH --mail-type=none
#SBATCH --mail-user=<userid>@rzg.mpg.de
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=72
#SBATCH --ntasks-per-core=1
#SBATCH -t 0:5:00
#SBATCH --partition=general


vibes run relaxation relaxation.in
