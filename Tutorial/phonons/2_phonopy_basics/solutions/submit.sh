#!/bin/bash -l

#SBATCH -J phonopy|vibes
#SBATCH -o log/phonopy.%j
#SBATCH -e log/phonopy.%j
#SBATCH -D ./
#SBATCH --mail-type=none
#SBATCH --mail-user=<userid>@rzg.mpg.de
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=72
#SBATCH --ntasks-per-core=1
#SBATCH -t 0:5:00
#SBATCH --partition=general


vibes run phonopy phonopy.in
