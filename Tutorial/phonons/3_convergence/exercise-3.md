# Exercise 3: Supercell Size Convergence

*Estimated total CPU time:  15-30 min*

!!! warning

    In the following exercises, computational settings including the reciprocal space grid (tag ```k_grid```), the basis set, and supercell's size, have been chosen to allow for a rapid computation of the exercises in the limited time and within the CPU resources available during the tutorial session. Without loss of generality, these settings allow to demonstrate trends of the lattice dynamics of materials.
    **In the production calculation, all computational parameters should be converged.**


!!! important "In this exercise, you will:"

* Perform phonon calculations in different supercells and inspect phonon dispersion convergence with respect to the supercell size.

## Supercell Size Convergence 
So far, we have only worked with a "supercell" that is equal to the
primitive unit cell. Although this calculation is not converged with
respect to the computational parameters, it already shows some
qualitative features of the band structure. The force constants are
typically more long ranged than the contribution from next nearest
neighbors as given in the primitive unit cell. In order to assess the
longer ranged contributions and investigate their influence on the
band structure, we now systematically increase the supercell size and
recompute the phonon properties.

Please go to the folder
```
cd phonons-with-fhi-vibes/Tutorial/phonons/3_convergence/input
```

to proceed with this task. You should copy `geometry.in` and `phonopy.in` for this exercise
[from the previous exercise](/tutorials/phonons-with-fhi-vibes/phonons/2_phonopy_basics/exercise-2/).

As already discussed, the particular supercell matrix we used in
exercise 2 corresponds to a unit cell matrix, thus the lattice of the
supercell is the same as the lattice of the primitive unit cell. The
ideal supercell size and shape depends on your problem at hand and it is
difficult to give definite advice. In practice, the supercell size needs
to be converged until the target property of interest is not changing
anymore. To facilitate this, *FHI-vibes* has an option to create
different supercells and moreover to determine a non-diagonal supercell
that is as cubic as possible, with the number of atoms close to the
specified value. In *Phonopy*, the crystal symmetry is automatically analyzed 
from the input unit cell structure file that does not contain the symmetry information. 
Symmetries are searched by attemping possible symmetry operations to the crystal structure 
and cheking if the crystal structure after the symmetry operation is overlapping to the original crystal structures. 
Generation of the 'as cubic as possible' supercell is beneficial,
because it leads to the smaller number of calculations once the supercell approach with the finite displacement method [^Parlinski] is used.
More details can be found in *Phonopy* reference papers [^Togo], [^Chaput].

To inspect *FHI-vibes* supercell generation functionality try to run:

```
vibes utils make-supercell -n 8
```

This would generate the conventional, cubic cell of silicon with 8
atoms:

```
...
Settings:
Target number of atoms: 8

Supercell matrix:
python:  [-1,  1,  1,  1, -1,  1,  1,  1, -1]
cmdline: -1 1 1 1 -1 1 1 1 -1
2d:
[[-1, 1, 1],
[1, -1, 1],
[1, 1, -1]]

Superlattice:
[[5.41718232 0.         0.        ]
 [0.         5.41718262 0.        ]
 [0.         0.         5.41718262]]

Number of atoms:  8
Cubicness:         1.000 (1.000)
Largest Cutoff:    2.709 AA
Number of displacements: 1 (1)

Supercell written to geometry.in.supercell_8
```

Consequently, the transformation matrix, which creates cubic supercell
(conventional Si cell) is: 

$$
\begin{aligned}
	\begin{pmatrix}  
	-1 & 1 & 1 \\  1 & -1 & 1 \\  1 & 1 & -1
	\end{pmatrix} 
\end{aligned}
$$ 

To run a calculation for such a
supercell, one just needs to replace the supercell keyword in the
`phonopy.in` section of *phonopy.in* file:

```
...
[phonopy]
supercell_matrix: [-1,  1,  1,  1, -1,  1,  1,  1, -1]
...
```

Before you run the calculations, you can try to compute the lattice of
the supercell by hand. Remember that the primitive FCC lattice is given
by:

$$
\begin{aligned}
	\begin{pmatrix}  
	0 & a/2 & a/2 \\  a/2 &  0 & a/2 \\  a/2 & a/2 &  0
	\end{pmatrix}~.
\end{aligned}
$$ 

!!! important What is the lattice you expect?
You may compute the volume of the supercell beforehand by
multiplying the *determinant* of the supercell matrix with the volume of
the primitive cell. In the same way, you can calculate the target number
of atoms in the supercell. (*Why?*) What is different from the primitive
unit cell? How many atoms are contained in the supercell and why? Is
your calculated lattice identical to the one produced by *FHI-vibes* ?
By default, *FHI-vibes* will write the supercell to a geometry input file named `geometry.in.supercell`, which you can
view e.g. with `jmol` or 'VESTA' visualization programs, e.g., via `jmol geometry.in.supercell`.


Perform the *Phonopy* calculation as before with

```
vibes run phonopy
```

The calculation will be performed in a folder named
`phonopy`, an automatically created
working directory triggered by setting

```
...
  [phonopy]
   workdir:            phonopy
  ...
```

in `phonopy.in`.

To increase the number of atoms, you could also try (this calculations should be done similarly to those above, but in a separate folder) the supercell matrix:

$$
\begin{aligned}
\begin{pmatrix}  
-2 & 2 & 2 \\  2 & -2 & 2 \\  2 & 2 & -2
\end{pmatrix}.
\end{aligned}
$$ 

How many atoms do you expect? In fact, once you increase the supercell size you should adjust your
`phonopy.in` accordingly and change the `k_grid` setting. For example, if you have a `k_grid` of $8\times8\times8$ 
for your primitive cell calculations and then you generate a $2\times2\times2$ supercell,
you should decrease `k_grid` to $4\times4\times4$.

!!! warning 
    Always check that you do not use extremely tight `k_grid` when you do calculations with the supercell, because this just burns computational resourses you have, without providing an additional accuracy.

The rational is the following: Since we *increased* the size of the
supercell, we *decreased* the size of the reciprocal space. This means:
If we do not change the `k_grid` settings, the number of
$\mathbf{k}$-points used to sample the reciprocal space stays constant
and the *density* of $\mathbf{k}$-points increases. To keep the density
of $\mathbf{k}$-points constant and thus consistent instead, we decrease
the number of $\mathbf{k}$-points. Always try to choose
$\mathbf{k}$-grids that are at least approximately commensurate when
working with supercells of different size to obtain consistent
results.[^1]. If you used `density` variable before, then everything would be adjusted
automatically, if you have used `k_grid` then adjust it to the supercell size.

Run the calculation. This calculation can take up to 10 minutes, depending on
your computer.

Reference for several other supercell shapes is given in the
folder `solutions`. Inspect the folder and use the provided
*FHI-aims* output files to compute the band structure of even larger
supercell. When do you consider your results as converged?

First, we show the result for the DOS and bandstructure of the 1\times1\times1$ conventional cell (8 atoms):

![](solutions/8_atoms/phonopy/output/bandstructure_dos.png)

Second, find below the same figure for the $2\times2\times2$ conventional cell (64 atoms):

![](solutions/64_atoms/phonopy/output/bandstructure_dos.png)

Changes in the DOS that can be visually observed are simple first check of convergence. We find rather significant changes going from $1\times1\times1$ to $2\times2\times2$ and we can conclude that the phonon properties of the $1\times1\times1$ conventional cell were not converged. Similarly, we would need to compute a larger supercell to cross-check if the $2\times2\times2$ conventional cell can be considered as converged.  

[^1]: The reason is that by changing the k-point density, the lattice constant as predicted by the DFT calculation might change and the lattice under consideration would be seen as slightly out-of-equilibrium. When the lattice constant is converged with respect to the k-points, this effect should be negligible. In any case, you should reduce the number of k-points as indicated to save
computation time, especially when working with large supercell. With FHI-aims however, going from $2\times2\times2$ to ($1\times1\times1$ mesh) does not decrease the computation time considerably.
FHI-vibes has a utility to suggest k-grids based on a density that can be used e. g. with `vibes utils suggest_k_grid geometry.in --density 5`.
When in doubt, this tool can be used to use consistent k-grid meshes.
[^Chaput]: [L. Chaput, A. Togo, I. Tanaka, and G. Hug, Phys. Rev. B, 84, 094302 (2011)](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.84.094302).
[^Parlinski]: [K. Parlinski, Z. Q. Li, and Y. Kawazoe, Phys. Rev. Lett. 78, 4063 (1997)](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.78.4063).
[^Togo]: [Atsushi Togo and Isao Tanaka, Scr. Mater., 108, 1-5 (2015)](https://www.sciencedirect.com/science/article/pii/S1359646215003127?via%3Dihub).
