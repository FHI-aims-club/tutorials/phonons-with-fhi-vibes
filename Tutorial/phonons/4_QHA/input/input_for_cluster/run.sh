logfile='run.log'

# timer
start_time="$(date -u +%s)"

for fol in qha_??.???
do echo Enter $fol
    cd $fol

    # perform phonopy calculation
    echo run phonopy
    vibes submit phonopy 

    # perform reference aims calculation
    echo run aims
    vibes submit singlepoint
    # next
    cd ..
done

