logfile='run.log'

# timer
start_time="$(date -u +%s)"

for fol in qha_??.???
do echo Enter $fol
    cd $fol

    # perform postprocess
    vibes output phonopy phonopy/trajectory.son --full

    # next
    cd ..
done

