#!/bin/bash -l

# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J bzu_test
# Queue (Partition):
#SBATCH --partition=general
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=72
#
#SBATCH --mail-type=none
#SBATCH --mail-user=<userid>@rzg.mpg.de
#
### for OpenMP:
###SBATCH --cpus-per-task=10

# Wall clock limit:
#SBATCH --time=00:05:00

#Run the program:
#ulimit -s unlimited
srun -n 72 /u/nrybin/BZU/FHIaims/build/aims.210513.scalapack.mpi.x > aims.out 
