# Preparations

## Download the tutorial material

If you are familiar with `git clone` you can simply clone the following repository:

```
git clone git@gitlab.com:FHI-aims-club/tutorials/phonons-with-fhi-vibes.git
```

Otherwise, you can download a tarball including all the tutorial material with this link:
```
https://gitlab.com/FHI-aims-club/tutorials/phonons-with-fhi-vibes/-/archive/main/phonons-with-fhi-vibes-main.tar
```

## Installation of FHI-aims

For this tutorial you need to have installed the FHI-aims version `210716_3` on the machine you will use for the computations.

1. We will use the `210716_3` release for this tutorial series. [Please download it from here.](https://fhi-aims.org/get-the-code-menu/downloads) 
2. Extract the tarball with:
  ```
  tar -xzf fhi-aims.210716_3.tgz
  ```
3. Follow the instructions on this page (you will need access to the FHI-aims GitLab): [https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial](https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial), or, alternatively, [download and read the manual](https://fhi-aims.org/get-the-code-menu/downloads).

## Installation and configuration of FHI-vibes

All necessary information about how to install FHI-vibes you can find here:

<https://vibes-developers.gitlab.io/vibes/Installation/>

We advise to install FHI-vibes in a separate python virtual environment on your local machine (e.g. using anaconda or virtualenv).

**Please read and follow the chapters Prerequisites, Installation, and Configuration of the FHI-vibes documentation.**

We now briefly summarize how to install FHI-vibes using anaconda on a local machine. We start by creating a new conda virtual environment

```
conda create --name fhi-vibes python=3
```

and activating it:

```
conda activate fhi-vibes
```

Afterwards, we need to install a fortran compiler in the new environment:

```
conda install -c conda-forge fortran-compiler
```

We can then install FHI-vibes using pip which also takes care of all dependencies for us:

```
pip install fhi-vibes
```

Before running the exercises, we still have to configure FHI-vibes:

```
vibes template configuration vibes > ~/.vibesrc
```

You now have to edit the `~/.vibesrc` specific to your system. Your file should resemble:

```
# System informations to get vibes running, please adjust:
# basissetloc: location of the FHIaims basissets
# aims_command: prefereably a script that runs aims, or the command to run it,
#                 e.g. `mpirun aims.x` or similar
#
# When adjusted, please copy to ~/.vibesrc

[machine]
basissetloc =      path/to/fhi-aims.210716_3/species_defaults/defaults_2020
aims_command =     mpirun -n 4 path/to/aims.210716_3.scalapack.mpi.x
```

Congratulations, you have installed FHI-vibes and can start the exercises!
